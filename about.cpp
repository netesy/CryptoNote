#include "about.h"
#include "includes.h"
#include "ui_about.h"


//set the text to use
const QString Coll =
        " <!DOCTYPE HTML >"
        "<html><head><meta name='qrichtext' content='1' /><style type='text/css'>"
        "p, li { white-space: pre-wrap; }"
        "</style></head><body style=' font-family:'MS Shell Dlg 2'; font-size:16pt; font-weight:400; font-style:normal;'>"
        "<p style=' margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;'>"
        "<span style=' font-size:16pt;color:#000000;'>Icons - </span><span style=' font-size:14pt;'>   </span><a href='http://Freepik.com'>"
        "<span style='font-size:16pt; text-decoration: underline; color:#0000ff;'>FreePik.com</span></a></p>"
        "<p style=' margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;'>"
        "<span style=' font-size:16pt;color:#000000;'>BigAes - </span><span style=' font-size:14pt;'>   </span>"
        " <a href='https://www.google.com/?gfe_rd=cr&amp;ei=92LUVZ7hKaWh8wf4iZCYCA&amp;gws_rd=ssl#q=BigAes'>"
        "<span style=' font-size:16pt; text-decoration: underline; color:#0000ff;'>Igor Saric</span></a></p>"
        "<p style=' margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;'>"
        "<span style=' font-size:16pt; color:#000000;'>Derivative</span><span style=' font-size:16pt; color:#000000;'> - </span>"
        " <a href='http://github.com/netesy/Derivative'><span style=' font-size:16pt; text-decoration: underline; color:#0000ff;'>Netesy Maximus</span></a></p></body></html>";
const QString Abouts =
        "<!DOCTYPE HTML>"
        "<html><head><meta name='qrichtext' content='1' /><style type='text/css'>"
        "p, li { white-space: pre-wrap; }"
        " </style></head><body style= 'font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;'>"
        " <p style=' margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;'>"
        "<span style= 'font-size:14pt; color:#0000ff;'>SecureNote is a Simple application that stores your notes securely "
        " and retrieves it securely using industry standard pratices.</span></p>"
        " <p style='-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:14pt; color:#0000ff;'><br /></p>"
        "  <p style='-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:14pt; color:#0000ff;'><br /></p></body></html> ";
const QString Credits =
        "<html><head><meta name='qrichtext' content='1'/></head><body>"
        "<table><tr><td>Created by:</td>"
        "<td><a href='mailto:netesy1@gmail.com'>Netesy Maximus</a></td></tr>"
        "<tr><td></td></tr></table></body></html>";
const QString License =
        "\n\nLICENSE TERMS\n\n"

        "The redistribution and use of this software (with or without changes)\n"
        "is allowed without the payment of fees or royalties provided that:\n\n"

        "1. source code distributions include the above copyright notice, this\n"
        "list of conditions and the following disclaimer;\n\n"

        "2. binary distributions include the above copyright notice, this list\n"
        "  of conditions and the following disclaimer in their documentation;\n\n"

        "3. the name of the copyright holder is not used to endorse products\n"
        "  built using this software without specific written permission.\n\n"

        " DISCLAIMER\n\n"

        " This software is provided 'as is' with no explicit or implied warranties\n"
        " in respect of its properties, including, but not limited to, correctness\n"
        " and/or fitness for purpose.";

About::About(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::About)
{
    ui->setupUi(this);
    ui->name->setText(QApplication::applicationName());
    ui->Version->setText(QApplication::applicationVersion());
    ui->organization->setText(QApplication::organizationName());
    ui->domain->setText(QApplication::organizationDomain());
    ui->textBrowser->hide();
}

About::~About()
{
    delete ui;
}

void About::closeEvent(QCloseEvent *event)
{
    Home *home = new Home();
    home->show();

}

void About::on_about_clicked()
{
    //show the textBrowser
    if(!ui->textBrowser->isHidden())
        ui->textBrowser->hide();
    else
        ui->textBrowser->show();
    //first clear the previous text if any
    ui->textBrowser->clear();
    //set the text
    ui->textBrowser->setText(Abouts);
    ui->textBrowser->append(Credits);
}

void About::on_license_clicked()
{
    //show the textBrowser
    if(!ui->textBrowser->isHidden())
        ui->textBrowser->hide();
    else
        ui->textBrowser->show();
    //first clear the previous text if any
    ui->textBrowser->clear();
    //set the text
    ui->textBrowser->setText(Coll);
    ui->textBrowser->append(License);
}
