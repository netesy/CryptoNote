#include "addnotes.h"
#include "ui_addnotes.h"
#include "includes.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QInputDialog>

AddNotes::AddNotes(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AddNotes)
{
    ui->setupUi(this);
}

AddNotes::~AddNotes()
{
    delete ui;
}

void AddNotes::closeEvent(QCloseEvent *event)
{
    Home *home = new Home();
    home->show();
}

void AddNotes::on_Submit_clicked()
{
    //set the sql settings
    QSqlDatabase db = QSqlDatabase::database();
    db.setDatabaseName(Includes::Path());
    if(!db.open())
    {
        QMessageBox amsg;
        amsg.setText("Unable To Connect To Database");
        amsg.exec();
    }
    QSqlQuery query(db);
    QString topic,note;
    topic = ui->lineEdit->text().toHtmlEscaped();
    note = ui->textBrowser->toHtml().toHtmlEscaped();
    //add the note to db
    QByteArray text,encrypted;
    //text(note.toUtf8());
    //start the aes engine
    //BigAES aes;
    this->setVisible(false);
    //set it
    bool ok;
    QString pass = QInputDialog::getText(this, tr("Password"),
                                         tr("Password:"), QLineEdit::Password,
                                         QDir::home().dirName(), &ok);
    if (ok && !pass.isEmpty())
    {
        QAesWrap aes(pass.toUtf8(),"Cryto",QAesWrap::AES_256);
        QByteArray mdata = aes.encrypt(note.toUtf8(),QAesWrap::AES_CTR);
        encrypted = mdata.toHex();
        //         encrypted =  aes.Encrypt(note.toUtf8(),pass.toUtf8());
    }
    this->setVisible(true);

    //decrypting is the opposite of encrypting
    // aes.Decrypt(encrypted,password);

    //this is where the note is wriiten from
    query.prepare(
                "INSERT INTO notes ( "
                "topic, note, date,time ) "
                "VALUES ( "
                ":topic, :note, :date, :time );"
                );
    query.bindValue(":topic", topic);
    query.bindValue(":note", encrypted);
    query.bindValue(":date",QDate::currentDate());
    query.bindValue(":time",QTime::currentTime());
    if( !query.exec() )
    {
        QMessageBox msg;
        msg.setText("Note Not Added successfulyy");
        msg.exec();
    }
    else
    {
    }
}
