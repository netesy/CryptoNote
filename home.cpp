#include "home.h"
#include "ui_home.h"
#include "includes.h"
#include <QtSql>


Home::Home(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Home)
{
    ui->setupUi(this);

    QSqlDatabase db = QSqlDatabase::database();
    db.setDatabaseName(Includes::Path());

    if(!db.open())
    {
        QMessageBox amsg;
        amsg.setText("Unable To Connect To Database");
        amsg.exec();
    }
    QSqlQueryModel *model = new QSqlQueryModel();
    model->setQuery("SELECT topic, id FROM notes");
    ui->listView->setModel(model);
}

Home::~Home()
{
    delete ui;
}

void Home::closeEvent(QCloseEvent *event)
{
    //later implementation
}

void Home::on_pushButton_clicked()
{
    //this is where the add notes code reside
    AddNotes *add = new AddNotes();
    this->hide();
    add->show();
    this->close();
}

void Home::on_pushButton_2_clicked()
{
    //this is where the delete notes code reside
}

void Home::on_actionAbout_triggered()
{
    About *about = new About();
    about->show();
    this->close();
}



void Home::on_actionSettings_triggered()
{
    Settings *set = new Settings();
    set->show();
    this->close();
}
