#ifndef SETTINGS_H
#define SETTINGS_H

#include <QWidget>

namespace Ui {
class Settings;
}

class Settings : public QWidget
{
    Q_OBJECT

public:
    explicit Settings(QWidget *parent = 0);
    void saveSettings();
    static QString readSettings();
    void readSetting();

    ~Settings();

private slots:
    void on_pushButton_clicked();
    void closeEvent(QCloseEvent *event);

private:
    Ui::Settings *ui;
};

#endif // SETTINGS_H
