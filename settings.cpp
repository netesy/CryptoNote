#include "settings.h"
#include "ui_settings.h"
#include "includes.h"


//global vars
QString file;


Settings::Settings(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);
}

void Settings::saveSettings()
{
    QSettings settings;
    settings.beginGroup("Look");
    settings.setValue("theme",file);
    settings.endGroup();
    settings.beginGroup("Network");
    settings.endGroup();

}

QString Settings::readSettings()
{
    QString out;
    QSettings settings;
    settings.beginGroup("Look");
    QString file = settings.value("theme").toString();
    out = file;
    settings.endGroup();
    return out;
}

void Settings::readSetting()
{
    QSettings settings;
    settings.beginGroup("Look");
    QString file = settings.value("theme").toString();
    this->ui->comboBox->setCurrentText(file);
    settings.endGroup();
}

Settings::~Settings()
{
    delete ui;
}

void Settings::on_pushButton_clicked()
{
    //    QString name = ui->comboBox->itemText(ui->comboBox->currentIndex());
    QString name = ui->comboBox->currentText();
    file = QString(":/theme/theme/%1.qss").arg(name);
    QFile qss(file);
    qss.open(QIODevice::ReadOnly);
    QString style = QLatin1String(qss.readAll());
    qApp->setStyleSheet(style);

    saveSettings();
    name.clear();
    file.clear();
    qss.flush();
    qss.close();
}

void Settings::closeEvent(QCloseEvent *event)
{
    Home *home = new Home();
    home->show();
}

