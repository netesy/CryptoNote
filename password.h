#ifndef PASSWORD_H
#define PASSWORD_H

#include <QWidget>

namespace Ui {
class Password;
}

class Password : public QWidget
{
    Q_OBJECT

public:
    explicit Password(QWidget *parent = 0);
    void SetPass(QString passy);
     QByteArray pass();
     QString passes;
    ~Password();

private slots:
    void on_pushButton_clicked();

private:
    Ui::Password *ui;
};

#endif // PASSWORD_H
