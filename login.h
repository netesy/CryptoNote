#ifndef LOGIN_H
#define LOGIN_H

#include <QWidget>

namespace Ui {
class Login;
}

class Login : public QWidget
{
    Q_OBJECT

public:
    explicit Login(QWidget *parent = 0);
    ~Login();

private slots:
    void closeEvent(QCloseEvent *event);
    void on_login_clicked();

    void on_Lost_clicked();

    void on_Register_clicked();

private:
    Ui::Login *ui;
};

#endif // LOGIN_H
