#ifndef ADDNOTES_H
#define ADDNOTES_H

#include <QWidget>

namespace Ui {
class AddNotes;
}

class AddNotes : public QWidget
{
    Q_OBJECT

public:
    explicit AddNotes(QWidget *parent = 0);
    ~AddNotes();

private slots:
    void closeEvent(QCloseEvent *event);
    void on_Submit_clicked();

private:
    Ui::AddNotes *ui;
};

#endif // ADDNOTES_H
