#include "login.h"
#include "ui_login.h"
#include "includes.h"

Login::Login(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Login)
{
    ui->setupUi(this);
}

Login::~Login()
{
    delete ui;
}

void Login::closeEvent(QCloseEvent *event)
{
    Home *home = new Home();
    home->show();
}

void Login::on_login_clicked()
{
    //here isn the login section

    //if validate open the main app
    this->close();
}

void Login::on_Lost_clicked()
{
    //tell the user that that feature is permitted on the free plan
    QMessageBox msg;
    msg.setText("Sorry that feature has \n not yet  been activated.\n what do you need it for??..");
    msg.exec();
}

void Login::on_Register_clicked()
{
    //take the user to the registeration page
    Register *reg =  new Register();
    this->hide();
    reg->show();
}
