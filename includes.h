#ifndef INCLUDES
#define INCLUDES
#include "addnotes.h"
#include "home.h"
#include "login.h"
#include "register.h"
#include "bigaes.h"
#include "qaeswrap.h"
#include "read.h"
#include "about.h"
#include "settings.h"
#include "password.h"
#include <QPalette>
#include <QStyle>
#include <QStyleFactory>
#include <QFile>
#include <QSql>
#include <QStandardPaths>
#include <QtCore>
#include <QMessageBox>
#include <QToolTip>
#include <QSqlDatabase>
#include <QDebug>
#include <QtGui>


class Includes
{


public:

    explicit Includes();
    static QString Path();
    ~Includes();

private:

};
#endif // INCLUDES

