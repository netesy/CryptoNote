#include "register.h"
#include "ui_register.h"
#include "includes.h"
#include <QtSql>
#include <QStandardPaths>
#include <QMessageBox>
#include <QDebug>

Register::Register(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Register)
{

    ui->setupUi(this);
}

void Register::Restart()
{
    ui->name->clear();
    ui->surname->clear();
    ui->username->clear();
    ui->radioButton->clearFocus();
    ui->password->clear();


}

Register::~Register()
{
    delete ui;
}

void Register::closeEvent(QCloseEvent *event)
{
    Home *home = new Home();
    home->show();

}

void Register::on_pushButton_clicked()
{
    QString name,surname,username,email,password;
    if(ui->name->text().isEmpty() || ui->password->text().isEmpty() || ui->surname->text().isEmpty() || ui->username->text().isEmpty() || ui->email->text().isEmpty())
    {
        QMessageBox msg;
        msg.setText("Please Fill all the Fields.");
        msg.exec();
    }else
    {
        name = ui->name->text().toHtmlEscaped();
        surname = ui->surname->text().toHtmlEscaped();
        username = ui->username->text().toHtmlEscaped();
        email = ui->email->text().toHtmlEscaped();
        password = ui->password->text().toHtmlEscaped();
    }

    {
        QSqlDatabase *db2 = new QSqlDatabase();
        *db2 = QSqlDatabase::database();
        db2->setDatabaseName(Includes::Path());
        if(!db2->open())
        {
            QMessageBox amsg;
            amsg.setText("Unable To Connect To Database");
            amsg.exec();
        }

        //Write the Tables to the database

        QSqlQuery query;
        query.prepare("CREATE TABLE IF NOT EXISTS user"
                      "(id INTEGER UNIQUE PRIMARY KEY,"
                      "name VARCHAR(40),"
                      "surname VARCHAR(40),"
                      "username VARCHAR(100),"
                      "email VARCHAR(160),"
                      "password VARCHAR(255));");
        if( !query.exec() )
        {
            QMessageBox msg;
            msg.setText("query 1 has an Error()");
            msg.exec();
        }
        else
        {
        }

        query.prepare("CREATE TABLE IF NOT EXISTS notes"
                      "(id INTEGER UNIQUE PRIMARY KEY,"
                      "topic TEXT,"
                      "note TEXT,"
                      "date DATE,"
                      "time TIME);");
        if( !query.exec() )
        {
            QMessageBox msg;
            msg.setText("query 2 has an Error()");
            msg.exec();

        }
        else
        {

        }
        //copy the information to the database

        query.prepare("INSERT INTO user ( "
                      "name, surname, username,email,password ) "
                      "VALUES ( "
                      ":name, :surname, :username, :email,:password );");
        query.bindValue(":name", name);
        query.bindValue(":surname", surname);
        query.bindValue(":username", username);
        query.bindValue(":email", email);
        query.bindValue(":password", password);
        if( !query.exec() )
        {
            QMessageBox msg;
            msg.setText("query 3 has an Error()");
            msg.exec();
           this->Restart();
        }
        else
        {
            //shows the home dialog after all is said and done
            this->close();

        }

    }

}

void Register::on_name_textChanged()
{

}

void Register::on_password_2_editingFinished()
{
    if(ui->password->text() == ui->password_2->text())
    {
        //
    }else
    {
        QString errorString = "Your Passwords Are Not The same!";
        QPoint point = QPoint(geometry().left() + ui->password_2->geometry().left(),
                              geometry().top() + ui->password_2->geometry().bottom());
        QToolTip::showText(point, errorString );
    }
}

void Register::on_password_2_textChanged()
{

    if(ui->password->text() == ui->password_2->text())
    {
        //
    }else
    {
        QString errorString = "Your Passwords Are Not The same!";
        QPoint point = QPoint(geometry().left() + ui->password_2->geometry().left(),
                              geometry().top() + ui->password_2->geometry().bottom());
        QToolTip::showText(point, errorString );
    }
}

void Register::on_password_2_textEdited()
{

    if(ui->password->text() == ui->password_2->text())
    {
        //
    }else
    {
        QString errorString = "Your Passwords Are Not The same!";
        QPoint point = QPoint(geometry().left() + ui->password_2->geometry().left(),
                              geometry().top() + ui->password_2->geometry().bottom());
        QToolTip::showText(point, errorString );
    }
}
