#ifndef REGISTER_H
#define REGISTER_H

#include <QWidget>

namespace Ui {
class Register;
}

class Register : public QWidget
{
    Q_OBJECT

public:
    explicit Register(QWidget *parent = 0);
    void Restart();
    ~Register();

private slots:
    void closeEvent(QCloseEvent *event);
    void on_pushButton_clicked();

    void on_name_textChanged();

    void on_password_2_editingFinished();

    void on_password_2_textChanged();

    void on_password_2_textEdited();

private:
    Ui::Register *ui;
};

#endif // REGISTER_H
