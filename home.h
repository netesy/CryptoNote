#ifndef HOME_H
#define HOME_H

#include <QMainWindow>

namespace Ui {
class Home;
}

class Home : public QMainWindow
{
    Q_OBJECT

public:
    explicit Home(QWidget *parent = 0);
    ~Home();

private slots:
    void closeEvent(QCloseEvent *event);
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_actionAbout_triggered();


    void on_actionSettings_triggered();

private:
    Ui::Home *ui;
};

#endif // HOME_H
