#-------------------------------------------------
#
# Project created by QtCreator 2015-08-01T14:45:03
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CryptoNote
TEMPLATE = app


SOURCES += main.cpp\
        home.cpp \
    login.cpp \
    register.cpp \
    addnotes.cpp \
    about.cpp \
    bigaes.cpp \
    read.cpp \
    settings.cpp \
    trest.cpp \
    includes.cpp \
    password.cpp

HEADERS  += home.h \
    login.h \
    register.h \
    addnotes.h \
    includes.h \
    about.h \
    bigaes.h \
    read.h \
    settings.h \
    trest.h \
    password.h

FORMS    += home.ui \
    login.ui \
    register.ui \
    addnotes.ui \
    about.ui \
    read.ui \
    settings.ui \
    password.ui

RESOURCES += \
    resources.qrc

include (./QAes/QAes.pri)
