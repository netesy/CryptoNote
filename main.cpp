#include "home.h"
#include <QApplication>
#include "includes.h"
#include <QMessageBox>
#include <QDir>


int main(int argc, char *argv[])
{
    QApplication::setApplicationName("CryptoNote");
    QApplication::setApplicationVersion("1.0.0a");
    QApplication::setOrganizationName("Majix");
    QApplication::setOrganizationDomain("http://majix.ml");
    QApplication::setDesktopSettingsAware(true);
    //create the db connection
    QSqlDatabase dbn = QSqlDatabase::addDatabase("QSQLITE");
    dbn.setDatabaseName(Includes::Path());
    QApplication::setStyle(QStyleFactory::create("Fusion"));
    QApplication a(argc, argv);
    QDir *name = new QDir();
    //if(name->mkpath(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)) != true)
    if(name->mkpath(QStandardPaths::writableLocation(QStandardPaths::DataLocation)) != true)
    {
        qDebug()<<"Couldn't create the path";
    }
    QFile qss(Settings::readSettings());
    qss.open(QIODevice::ReadOnly);
    qApp->setStyleSheet(qss.readAll());

    //check if database exist
    QFile Fout(Includes::Path());
    Fout.open(QIODevice::ReadOnly);
    //if its not existing let the user Register
    if(!Fout.exists())
    {
        Register *reg = new Register();
        reg->show();
    }
    else
    {
        //if the user is already registered let him login
        Login *log = new Login();
        log->show();
    }


    return a.exec();

}
