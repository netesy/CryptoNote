#include "password.h"
#include "ui_password.h"

Password::Password(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Password)
{
    ui->setupUi(this);
}

void Password::SetPass(QString passy)
{
 this->passes = passy;
}

QByteArray Password::pass()
{
    return this->passes.toUtf8();
}

Password::~Password()
{
    delete ui;
}

void Password::on_pushButton_clicked()
{
    //get the files and send it to a static object
   QString key = ui->lineEdit->text();
   this->SetPass(key);
   //connect(pushButton, SIGNAL(on_pushButton_clicked()), this, SLOT());
   ui->lineEdit->clear();
   this->close();
}
