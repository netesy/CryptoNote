# CryptoNote
An AES Encrypting Note Taking Application.
It's written with Qt/C++ and its cross platform confirmed to compile succesfully on windows 8 and Ubuntu 14.04
If Possible i would love to See Images of it working in Apple

## How it Works
* the users create an for authorisation to CREATE READ DELETE files to the Application
* When a new Note is created, the user hashes it with a password selected at the time of submission
* The Note password is not stored in the application, thereby increasing security

###    TODO

* implement synchronization to Dropbox
* Implement a fanfiction chapter like system for Story Authors
* Implement a Wyswyg Editor for writing new notes

##  Support
Please any form of support is welcomed
